// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"

// Allocates more memory for TPC files and changes the TPC pointer to the newly allocated memory
void allocateAdditionalTextureMemory(HANDLE phandle) {
	int newTpcAddress = 0x10050000;
	int tpcAddressPointerAddress = 0x004753C8;
	int tpcAllocationSize = 0x3B9AC9FF;

	// Allocate memory
	for (int i = 0; i < 10; i++) {
		VirtualAlloc(reinterpret_cast<LPVOID>(newTpcAddress + tpcAllocationSize * i), tpcAllocationSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	}
	// Changes pointer to newly allocated memory
	WriteProcessMemory(phandle, reinterpret_cast<LPVOID>(tpcAddressPointerAddress), &newTpcAddress, sizeof(newTpcAddress), NULL);
}

// Disables some rendering calls by setting them to nop
// When these instructions are set to nop, the game engine will always render level meshes
// When not set to nop, the game engine will hide certain level meshes depending on the camera's view of them
void fixMeshRendering(HANDLE phandle) {
	int renderInstructionAddress0 = 0x0049F6DF;
	int renderInstructionAddress1 = 0x0049F80F;
	short nopShort = 0x9090;
	char nopByte = 0x90;
	WriteProcessMemory(phandle, reinterpret_cast<LPVOID>(renderInstructionAddress0), &nopShort, sizeof(nopShort), NULL);

	for (int iNop = 0; iNop < 6; iNop++) {
		WriteProcessMemory(phandle, reinterpret_cast<LPVOID>(renderInstructionAddress1 + iNop), &nopByte, sizeof(nopByte), NULL);
	}
}

// Disables a texture call by setting it to nop
// When this instruction is set to nop, the game loads HD textures perfectly fine
// When the instruction is not set to nop, the game will crash when loading an HD texture
void applyBigTextureFix(HANDLE phandle) {
	int textureInstructionAddress = 0x0046B379;
	char nopByte = 0x90;

	for (int iNop = 0; iNop < 6; iNop++) {
		WriteProcessMemory(phandle, reinterpret_cast<LPVOID>(textureInstructionAddress + iNop), &nopByte, sizeof(nopByte), NULL);
	}
}

__declspec(dllexport) void start() {
	HANDLE phandle = OpenProcess(PROCESS_ALL_ACCESS, 0, GetCurrentProcessId());
	allocateAdditionalTextureMemory(phandle);
	fixMeshRendering(phandle);
	applyBigTextureFix(phandle);
}

void start();

__declspec(dllexport) BOOL DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		start();
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

__declspec(dllimport) BOOL DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
);